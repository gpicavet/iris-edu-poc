
import com.ctc.wstx.stax.WstxInputFactory;
import com.ctc.wstx.stax.WstxOutputFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.XmlFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.jms.*;
import java.io.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class GenerateTraces {

    String amqUrl;
    String logstashUrl;

    Connection connection;
    Session session;
    MessageProducer producer;

    CloseableHttpClient httpclient;

    XmlMapper mapper;

    GenerateTraces(String amqUrl, String amqQueue, String logstashUrl) throws Exception {
        this.amqUrl=amqUrl;
        this.logstashUrl=logstashUrl;
        try {
            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();

            // Create a Connection
            connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination (Topic or Queue)
            Destination destination = session.createQueue(amqQueue);

            // Create a MessageProducer from the Session to the Topic or Queue
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);


            XmlFactory factory = new XmlFactory(new WstxInputFactory(), new WstxOutputFactory());
            mapper = new XmlMapper(factory);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

            httpclient = HttpClients.createDefault();
        } catch(Exception e) {
            clean();
            throw e;
        }
    }

    void clean() throws Exception {
        connection.close();
        session.close();
        httpclient.close();
    }

    void start(String dateFromStr, String dateToStr, int nInstancesFluxPerDays, int nFluxPerSec) throws Exception {

        try {
            ReadDefinitionFlux readDefinitionFlux = new ReadDefinitionFlux().invoke();
            Map<String, DefinitionFlux> definitionsDeFlux = readDefinitionFlux.getDefinitionsDeFlux();
            List<String> codesFlux = readDefinitionFlux.getCodesFlux();


//            execHttp(new HttpDelete(logstashUrl+"/traces-*"));



            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            DateTimeFormatter dtfIso = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
            DateTimeFormatter dtfIdFlux = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

            LocalDate dateFrom = LocalDate.parse(dateFromStr, dtf);
            LocalDate dateTo = LocalDate.parse(dateToStr, dtf);



            double[] distribCP_PC = {0.5, 1};
            double[] distribNDemiFluxOut_CP = {0, 0.5, 0.75, 0.9, 0.95, 0.96, 0.97, 0.98, 0.99, 0.995, 1};
            double[] distribFlux_CP = {0.3, 0.55, 0.75, 0.8, 0.85, 0.9, 0.95, 1};
            String[] distribFluxName_CP = {"U03","ECO02","U05","U16","LIS51","U20","U02"};
            double[] distribStatus = {0.00005,0.99995, 1}; //encours,ok,ko

            Random random = new Random();

            int nFlux = 1;


            while (!dateFrom.isAfter(dateTo)) {

                System.out.println(dtf.format(dateFrom));

                ZonedDateTime dateTimeFrom = dateFrom.atTime(0, 0).atZone(ZoneId.systemDefault());

                for (int fl = 0; fl < nInstancesFluxPerDays; fl++) {

                    int status = probaPartition(distribStatus);

                    boolean isCP = probaPartition(distribCP_PC) == 0;

                    if (isCP) {
                        String codeFlux = null;
                        DefinitionDemiFlux defDemiFluxIn = null;
                        DefinitionDemiFlux defDemiFluxOut = null;
                        //recherche d'un flux compatible, en tenant compte des distributions
                        for (;;) {
                            int index = probaPartition(distribFlux_CP);
                            if(index==distribFlux_CP.length-1) {
                                codeFlux = codesFlux.get(random.nextInt(codesFlux.size()));
                            } else {
                                codeFlux = distribFluxName_CP[index];
                            }
                            defDemiFluxIn = definitionsDeFlux.get(codeFlux).mapDemiFlux.get("APP_EDU");
                            defDemiFluxOut = definitionsDeFlux.get(codeFlux).mapDemiFlux.get("EDU_PDV");
                            if (defDemiFluxIn != null && defDemiFluxOut != null)
                                break;
                        }
                        String codePartenaireIn = defDemiFluxIn.codesPartenaires.get(random.nextInt(defDemiFluxIn.codesPartenaires.size()));
                        DefinitionPartenaire defPartIn = defDemiFluxIn.mapPartenaires.get(codePartenaireIn);
                        String serveur = defPartIn.serveurs.get(random.nextInt(defPartIn.serveurs.size()));

                        //generation d'un flux sur cette journée.
                        ZonedDateTime dateHeureIn = dateTimeFrom.plusSeconds(random.nextInt(24 * 3600));
                        // La date du 1er 1/2 flux sortant se produit jusqu'a qq heures apres le démarrage du 1/2 flux entrant
                        ZonedDateTime dateHeureOut = dateHeureIn.plusSeconds(random.nextInt(6 * 3600));

                        ZonedDateTime dateTrace = dateHeureIn.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                        Trace traceIn1 = new Trace();
                        traceIn1.flux = codeFlux;
                        traceIn1.demiFlux = "APP_EDU";
                        traceIn1.IdPointCollecte = "DEMARRAGE";
                        traceIn1.dateHeureCollecte = dtfIso.format(dateTrace);
                        traceIn1.fichier = traceIn1.flux + String.format("%5s", nFlux).replace(' ', '0') + ".01";
                        traceIn1.idInstanceFlux = "FIC40000_" + dtfIdFlux.format(dateTrace) + "_" + traceIn1.fichier;
                        traceIn1.natureTrace = "SUPERV";
                        traceIn1.typeTrace = "TECH";
                        traceIn1.partenaire = "01";
                        traceIn1.anteServeur = serveur;
                        Info infoHostname = new Info();
                        infoHostname.clef = "HOSTNAME";
                        infoHostname.valeur = serveur;
                        traceIn1.infos.add(infoHostname);
                        sendTrace(traceIn1);

                        dateTrace = dateTrace.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                        Trace traceIn2 = new Trace(traceIn1);
                        traceIn2.IdPointCollecte = "DIFFUSION";
                        traceIn2.dateHeureCollecte = dtfIso.format(dateTrace);
                        sendTrace(traceIn2);

                        dateTrace = dateTrace.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                        Trace traceIn3 = new Trace(traceIn1);
                        traceIn3.IdPointCollecte = "TERMINE_OK";
                        traceIn3.dateHeureCollecte = dtfIso.format(dateTrace);
                        traceIn3.datePeremption = dtfIso.format(dateTrace.plusDays(1));
//                    Info infocd = new Info();
//                    infocd.clef = "CD_ASSOCIE";
//                    infocd.valeur = "xxxx";
//                    traceIn3.infos.add(infocd);
                        sendTrace(traceIn3);

                        int nDemiFluxOut = probaPartition(distribNDemiFluxOut_CP);
                        for (int ndf = 0; ndf < nDemiFluxOut; ndf++) {

                            String codePartenaire = defDemiFluxOut.codesPartenaires.get(random.nextInt(defDemiFluxOut.codesPartenaires.size()));
                            DefinitionPartenaire defPartOut = defDemiFluxOut.mapPartenaires.get(codePartenaire);

                            dateTrace = dateHeureOut.plus(random.nextInt(3600 * 1000), ChronoUnit.MILLIS);
                            Trace traceOut1 = new Trace(traceIn3);
                            traceOut1.IdPointCollecte = "DEMARRAGE";
                            traceOut1.demiFlux = "EDU_PDV";
                            traceOut1.dateHeureCollecte = dtfIso.format(dateTrace);
                            traceOut1.datePeremption = dtfIso.format(dateTrace.plusDays(1));
                            traceOut1.idInstanceDemiFlux = codePartenaire;
                            traceOut1.cdAssocie = codePartenaire;
                            Info infocd = new Info();
                            infocd.clef = "CD_ASSOCIE";
                            infocd.valeur = codePartenaire;
                            traceOut1.infos.add(infocd);

                            sendTrace(traceOut1);

                            if(status >0) {
                                dateTrace = dateTrace.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                                Trace traceOut2 = new Trace(traceOut1);
                                traceOut2.IdPointCollecte = "DISTRIBUTION";
                                traceOut2.dateHeureCollecte = dtfIso.format(dateTrace);
                                sendTrace(traceOut2);

                                dateTrace = dateTrace.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                                Trace traceOut3 = new Trace(traceOut1);
                                if (status == 1) {
                                    traceOut3.IdPointCollecte = "TERMINE_OK";
                                } else {
                                    traceOut3.IdPointCollecte = "TERMINE_KO";

                                    Info infoCodeErr = new Info();
                                    infoCodeErr.clef = "CODE_ERREUR";
                                    infoCodeErr.valeur = "3118";
                                    traceOut3.infos.add(infoCodeErr);
                                    Info infoMsgErr = new Info();
                                    infoMsgErr.clef = "MESSAGE_ERREUR";
                                    infoMsgErr.valeur = "E2015: Refus : fichier déjà existant";
                                    traceOut3.infos.add(infoMsgErr);
                                }
                                traceOut3.dateHeureCollecte = dtfIso.format(dateTrace);
                                sendTrace(traceOut3);
                            }
                        }
                    } else {


                        String codeFlux = null;
                        DefinitionDemiFlux defDemiFluxIn = null;
                        DefinitionDemiFlux defDemiFluxOut = null;
                        //recherche d'un flux compatible
                        for (; ; ) {
                            codeFlux = codesFlux.get(random.nextInt(codesFlux.size()));
                            defDemiFluxIn = definitionsDeFlux.get(codeFlux).mapDemiFlux.get("PDV_EDU");
                            defDemiFluxOut = definitionsDeFlux.get(codeFlux).mapDemiFlux.get("EDU_APP");
                            if (defDemiFluxIn != null && defDemiFluxOut != null)
                                break;
                        }
                        String codePartenaireIn = defDemiFluxIn.codesPartenaires.get(random.nextInt(defDemiFluxIn.codesPartenaires.size()));
                        DefinitionPartenaire defPartIn = defDemiFluxIn.mapPartenaires.get(codePartenaireIn);
                        String serveur = defPartIn.serveurs.get(random.nextInt(defPartIn.serveurs.size()));

                        //generation d'un flux sur cette journée.
                        ZonedDateTime dateHeureIn = dateTimeFrom.plusSeconds(random.nextInt(24 * 3600));
                        // La date du 1er 1/2 flux sortant se produit jusqu'a qq heures apres le démarrage du 1/2 flux entrant
                        ZonedDateTime dateHeureOut = dateHeureIn.plusSeconds(random.nextInt(6 * 3600));

                        ZonedDateTime dateTrace = dateHeureIn.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                        Trace traceIn1 = new Trace();
                        traceIn1.flux = codeFlux;
                        traceIn1.demiFlux = "PDV_EDU";
                        traceIn1.IdPointCollecte = "DEMARRAGE";
                        traceIn1.dateHeureCollecte = dtfIso.format(dateTrace);
                        traceIn1.fichier = traceIn1.flux + String.format("%5s", nFlux).replace(' ', '0') + ".01";
                        traceIn1.idInstanceFlux = "FIC40000_" + dtfIdFlux.format(dateTrace) + "_" + traceIn1.fichier;
                        traceIn1.natureTrace = "SUPERV";
                        traceIn1.typeTrace = "TECH";
                        traceIn1.partenaire = "01";
                        traceIn1.anteServeur = serveur;
                        Info infoHostname = new Info();
                        infoHostname.clef = "HOSTNAME";
                        infoHostname.valeur = serveur;
                        traceIn1.infos.add(infoHostname);
                        sendTrace(traceIn1);

                        dateTrace = dateTrace.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                        Trace traceIn2 = new Trace(traceIn1);
                        traceIn2.IdPointCollecte = "DIFFUSION";
                        traceIn2.dateHeureCollecte = dtfIso.format(dateTrace);
                        sendTrace(traceIn2);

                        dateTrace = dateTrace.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                        Trace traceIn3 = new Trace(traceIn1);
                        traceIn3.IdPointCollecte = "TERMINE_OK";
                        traceIn3.dateHeureCollecte = dtfIso.format(dateTrace);
                        traceIn3.datePeremption = dtfIso.format(dateTrace.plusDays(1));
//                    Info infocd = new Info();
//                    infocd.clef = "CD_ASSOCIE";
//                    infocd.valeur = "xxxx";
//                    traceIn3.infos.add(infocd);
                        sendTrace(traceIn3);

                        int nDemiFluxOut = 1;
                        for (int ndf = 0; ndf < nDemiFluxOut; ndf++) {

                            String codePartenaire = defDemiFluxOut.codesPartenaires.get(random.nextInt(defDemiFluxOut.codesPartenaires.size()));
                            DefinitionPartenaire defPartOut = defDemiFluxOut.mapPartenaires.get(codePartenaire);

                            dateTrace = dateHeureOut.plus(random.nextInt(3600 * 1000), ChronoUnit.MILLIS);
                            Trace traceOut1 = new Trace(traceIn3);
                            traceOut1.IdPointCollecte = "DEMARRAGE";
                            traceOut1.demiFlux = "EDU_APP";
                            traceOut1.dateHeureCollecte = dtfIso.format(dateTrace);
                            traceOut1.datePeremption = dtfIso.format(dateTrace.plusDays(1));
                            traceOut1.idInstanceDemiFlux = codePartenaire;
                            traceOut1.cdAssocie = codePartenaire;
                            Info infocd = new Info();
                            infocd.clef = "CD_ASSOCIE";
                            infocd.valeur = codePartenaire;
                            traceOut1.infos.add(infocd);

                            sendTrace(traceOut1);

                            if(status >0) {
                                dateTrace = dateTrace.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                                Trace traceOut2 = new Trace(traceOut1);
                                traceOut2.IdPointCollecte = "DISTRIBUTION";
                                traceOut2.dateHeureCollecte = dtfIso.format(dateTrace);
                                sendTrace(traceOut2);

                                dateTrace = dateTrace.plus(random.nextInt(5 * 1000), ChronoUnit.MILLIS);
                                Trace traceOut3 = new Trace(traceOut1);
                                if (status == 1) {
                                    traceOut3.IdPointCollecte = "TERMINE_OK";
                                } else {
                                    traceOut3.IdPointCollecte = "TERMINE_KO";

                                    Info infoCodeErr = new Info();
                                    infoCodeErr.clef = "CODE_ERREUR";
                                    infoCodeErr.valeur = "3118";
                                    traceOut3.infos.add(infoCodeErr);
                                    Info infoMsgErr = new Info();
                                    infoMsgErr.clef = "MESSAGE_ERREUR";
                                    infoMsgErr.valeur = "E2015: Refus : fichier déjà existant";
                                    traceOut3.infos.add(infoMsgErr);
                                }
                                traceOut3.dateHeureCollecte = dtfIso.format(dateTrace);
                                sendTrace(traceOut3);
                            }
                        }

                    }

                    nFlux++;

                    if(nFlux % nFluxPerSec == 0) {
                        System.out.println("WAITING 1 sec...");
                        Thread.sleep(1000);
                    }
                }

                dateFrom = dateFrom.plusDays(1);

            }
        } finally {
            clean();
        }

    }


    private void sendTrace(Trace traceIn) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        mapper.writeValue(baos, traceIn);

        //System.out.println(baos.toString());

        TextMessage msg = session.createTextMessage(baos.toString());
        producer.send(msg);
    }

    private void execHttp(HttpUriRequest request) {
        try (CloseableHttpResponse response = httpclient.execute(request)) {
            System.out.println(response.getStatusLine().toString());
            System.out.println(new Scanner(response.getEntity().getContent(),"utf-8").useDelimiter("\\Z").next());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private int probaPartition(double[] distrib) {
        double r = Math.random();
        for(int i=0; i<distrib.length; i++) {
            if(r<distrib[i])
                return i;
        }
        return distrib.length-1;
    }

    static final String NS = "http://systemu/eai/socle/supervisionedu/trace";

    @JacksonXmlRootElement(localName = "trace", namespace = NS)
    class Trace {
        @JacksonXmlProperty(namespace = NS)
        public String flux;
        @JacksonXmlProperty(namespace = NS)
        public String demiFlux;
        @JacksonXmlProperty(namespace = NS)
        public String IdPointCollecte;
        @JacksonXmlProperty(namespace = NS)
        public String idInstanceFlux;
        @JacksonXmlProperty(namespace = NS)
        public String idInstanceDemiFlux;
        @JacksonXmlProperty(namespace = NS)
        public String dateHeureCollecte;
        @JacksonXmlProperty(namespace = NS)
        public String natureTrace;
        @JacksonXmlProperty(namespace = NS)
        public String typeTrace;
        @JacksonXmlProperty(namespace = NS)
        public String partenaire;
        @JacksonXmlProperty(namespace = NS)
        public String cdAssocie;
        @JacksonXmlProperty(namespace = NS)
        public String anteServeur;
        @JacksonXmlProperty(namespace = NS)
        public String fichier;
        @JacksonXmlProperty(namespace = NS)
        public String datePeremption;
        @JacksonXmlProperty(namespace = NS)
        public List<Info> infos = new ArrayList<>();

        public Trace() {}

        public Trace(Trace t) {
            this.flux = t.flux;
            this.demiFlux = t.demiFlux;
            this.IdPointCollecte = t.IdPointCollecte;
            this.idInstanceFlux = t.idInstanceFlux;
            this.idInstanceDemiFlux = t.idInstanceDemiFlux;
            this.dateHeureCollecte = t.dateHeureCollecte;
            this.natureTrace = t.natureTrace;
            this.typeTrace = t.typeTrace;
            this.partenaire = t.partenaire;
            this.cdAssocie = t.cdAssocie;
            this.anteServeur = t.anteServeur;
            this.fichier = t.fichier;
            this.datePeremption = t.datePeremption;
            this.infos = new ArrayList<>();
            for(Info i : t.infos) {
                this.infos.add(new Info(i));
            }
        }
    }

    class Info {
        @JacksonXmlProperty(namespace = NS)
        public String clef;
        @JacksonXmlProperty(namespace = NS)
        public String valeur;

        public Info() {}
        public Info(Info i) {
            this.clef = i.clef;
            this.valeur = i.valeur;
        }

    }

    class DefinitionFlux {
        public String code;
        public Map<String, DefinitionDemiFlux> mapDemiFlux = new HashMap<>();
    }

    class DefinitionDemiFlux {
        public String code;
        public Map<String, DefinitionPartenaire> mapPartenaires = new HashMap<>();
        public List<String> codesPartenaires;
    }

    class DefinitionPartenaire {
        public String code;
        public String libelle;
        public List<String> serveurs = new ArrayList<>();
    }

    class ReadDefinitionFlux {
        private Map<String, DefinitionFlux> definitionsDeFlux;
        private List<String> codesFlux;

        public Map<String, DefinitionFlux> getDefinitionsDeFlux() {
            return definitionsDeFlux;
        }

        public List<String> getCodesFlux() {
            return codesFlux;
        }

        public ReadDefinitionFlux invoke() throws IOException {
            definitionsDeFlux = new HashMap<>();
            codesFlux = null;

            BufferedReader reader = new BufferedReader(new InputStreamReader(GenerateTraces.class.getResourceAsStream("definitionDeFlux.csv")));
            String line;
            line = reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] row = line.split("[\t]+");
                try {
                    String codeFlux = row[4].substring(1, row[4].length() - 1);
                    String codeDemiFlux = row[1].substring(1, row[1].length() - 1);
                    String nmServeur = row[5].substring(1, row[5].length() - 1);
                    String codePartenaire = row[0].substring(1, row[0].length() - 1);
                    String libPartenaire = row[6].substring(1, row[6].length() - 1);

                    DefinitionFlux def = definitionsDeFlux.get(codeFlux);
                    if (def == null) {
                        def = new DefinitionFlux();
                        def.code = codeFlux;
                        definitionsDeFlux.put(codeFlux, def);
                        codesFlux = new ArrayList(definitionsDeFlux.keySet());
                    }
                    DefinitionDemiFlux defd = def.mapDemiFlux.get(codeDemiFlux);
                    if (defd == null) {
                        defd = new DefinitionDemiFlux();
                        defd.code = codeDemiFlux;
                        def.mapDemiFlux.put(codeDemiFlux, defd);
                    }
                    DefinitionPartenaire defp = defd.mapPartenaires.get(codePartenaire);
                    if (defp == null) {
                        defp = new DefinitionPartenaire();
                        defp.code = codePartenaire;
                        defp.libelle = libPartenaire;
                        if (!defp.serveurs.contains(nmServeur))
                            defp.serveurs.add(nmServeur);
                        defd.mapPartenaires.put(codePartenaire, defp);
                        defd.codesPartenaires = new ArrayList(defd.mapPartenaires.keySet());
                    }

                } catch (Exception e) {
                    System.out.println("WARN"+e.getMessage());
                }
            }
            return this;
        }
    }


    public static void main(String[] args) throws Exception {

        String dateFromStr=args[0];
        String dateToStr=args[1];
        int nFluxPerSec=args.length==3 ? Integer.parseInt(args[2]) : Integer.MAX_VALUE;

        GenerateTraces gen = new GenerateTraces("tcp://localhost:61616", "TEST.TRACES","http://localhost:19200");
        gen.start(dateFromStr, dateToStr, 100000, nFluxPerSec);

    }
}
