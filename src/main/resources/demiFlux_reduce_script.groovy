def demiFluxAggList=[];
def docByDemiFluxMap=new HashMap();
def nbdemifluxSortant=0,nbdemifluxEntrant=0,nbdemifluxtot=0;

for(docs in states) {
  for(doc in docs) {
    def id=doc.get('idInstanceDemiFlux');
    def demiFluxAgg = docByDemiFluxMap.get(id);

      if ( demiFluxAgg == null) {
          demiFluxAgg = new HashMap();
          demiFluxAgg.put('idInstanceDemiFlux', id);
          demiFluxAgg.put('demiFlux', doc.get('demiFlux'));

          if (doc.get('cdAssocie') != null) {
              demiFluxAgg.put('cdAssocie', doc.get('cdAssocie'));
          }

          docByDemiFluxMap.put(id, demiFluxAgg);
          demiFluxAggList.add(demiFluxAgg);

        if(doc.get('demiFlux').startsWith('EDU'))
            nbdemifluxSortant++;
        else
            nbdemifluxEntrant++;

        nbdemifluxtot = nbdemifluxEntrant + nbdemifluxSortant;
    }

    if ( doc.get('IdPointCollecte')=='DEMARRAGE') {
      demiFluxAgg.put('dateHeureCollecte',doc.get('dateHeureCollecte'));
    }

    if ( demiFluxAgg.get('maxTime') == null || demiFluxAgg.get('maxTime') < doc.get('dateHeureCollecte').toInstant().toEpochMilli()) {
      demiFluxAgg.put('maxTime', doc.get('dateHeureCollecte').toInstant().toEpochMilli());
      demiFluxAgg.put('idEtatInstance',doc.get('idEtatInstance'));
    }
  }
}


for(demiFluxAgg in demiFluxAggList) {
    if (nbdemifluxtot == 1 && nbdemifluxEntrant == 1 && demiFluxAgg.get('idEtatInstance').equals('1')) demiFluxAgg.put('idEtatInstance', 2);//on reste en cours si juste un entrant OK
    if (nbdemifluxtot == 1 && nbdemifluxSortant == 1) demiFluxAgg.put('idEtatInstance', 3);//tous les 1/2 flux en erreur si juste un sortant
    if (nbdemifluxtot > 2 && nbdemifluxEntrant > 1 && nbdemifluxSortant == 1) demiFluxAgg.put('idEtatInstance', 3);//tous les 1/2 flux en erreur si plusieurs sortants
}

def idEtatInstance='1';

for(demiFluxAgg in demiFluxAggList) { if(demiFluxAgg.get('idEtatInstance').equals('2')) idEtatInstance='2';  }
for(demiFluxAgg in demiFluxAggList) { if(demiFluxAgg.get('idEtatInstance').equals('3')) idEtatInstance='3';  }

def res = new HashMap();
res.put('idEtatInstance',idEtatInstance);
res.put('list',demiFluxAggList);
return res