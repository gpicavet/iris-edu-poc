/*
 * lancement :
 * docker run --rm -v "%cd%/src/test/k6":/scripts -e TARGET=http://poc-iris.westeurope.cloudapp.azure.com:19200 loadimpact/k6 run --vus 1 --iterations 1 "/scripts/listeFlux.js"
 */
import http from "k6/http";
import {check} from "k6";

export default function () {

    let URL = __ENV.TARGET;

    let od = Math.floor(Math.random() * 80*24*3600*1000);
    let of = Math.floor(Math.random() * (80*24*3600*1000 - od));
    let dateDebut = new Date(Date.parse("2020-01-01").getTime()+od);
    let dateFin = new Date(dateDebut.getTime()+of);

    let sens= Math.random()>0.5 ? "C-P" : "P-C";

    let requestBody =
    {
       "query":{
          "bool": {
            "filter": [
              {"range":{
                 "dateHeureDebut":{
                    "gte":dateDebut.toISOString(),
                    "lt":dateFin.toISOString()
                 }
              }},
              {"term": {
                "sens.keyword":sens
              }}
            ]
          }
       },
       "size":0,
       "aggs":{
          "by_flux":{
             "terms":{
                "field":"flux.keyword",
                "size": 1000,
                "order" : { "ko" : "desc" }
             },
             "aggs":{
                "by_state":{
                   "terms":{
                      "field":"demiFlux.idEtatInstance.keyword"
                   }
                },
                "ko": {
                  "filter": {"term": { "demiFlux.idEtatInstance.keyword" : "3" }}
                  },
                "minDateHeureDebut": {
                  "min": { "field": "dateHeureDebut" }
                 },
                "maxDateHeureFin": {
                  "max": { "field": "dateHeureFin" }
                 }
             }
          }
       }
    }

    let res = http.post(URL + "/traces_agg*/_search", JSON.stringify(requestBody), {headers:{"Content-Type":"application/json"}});

    check(res, {"status OK" : ()=> res.status == 200});
}
