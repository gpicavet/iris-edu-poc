import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.util.Scanner;

public class PutTransform {
    public static void main(String[] args) throws Exception {
        String URL = "http://localhost:19200";
        
        try(CloseableHttpClient httpclient = HttpClients.createDefault()) {

            HttpDelete deleteIndex = new HttpDelete(URL + "/traces_agg*");
            execHttp(httpclient, deleteIndex);

            HttpDelete deletePipeline = new HttpDelete(URL + "/_ingest/pipeline/traces_agg");
            execHttp(httpclient, deletePipeline);

            HttpPut putPipeline = new HttpPut(URL + "/_ingest/pipeline/traces_agg");
            putPipeline.setHeader("Content-Type","application/json");
            putPipeline.setEntity(new InputStreamEntity(PutTransform.class.getResourceAsStream("/pipeline.json")));
            execHttp(httpclient, putPipeline);

            HttpDelete deleteTransform = new HttpDelete(URL + "/_transform/traces_agg?force");
            execHttp(httpclient, deleteTransform);

            String json = new Scanner(PutTransform.class.getResourceAsStream("/transform.json")).useDelimiter("\\Z").next();
            String newJson = json;
            for(String script: new String[]{"demiFlux_map_script", "demiFlux_reduce_script"}) {
                String groovy = new Scanner(PutTransform.class.getResourceAsStream("/"+script+".groovy")).useDelimiter("\\Z").next();
                groovy = groovy.replaceAll("\r"," ");
                groovy = groovy.replaceAll("\n","\\\\n");
                groovy = groovy.replaceAll("\"","\\\"");
                newJson = newJson.replace("{{ "+script+" }}", groovy);
            }
           System.out.println(newJson);


            HttpPut putTransform = new HttpPut(URL + "/_transform/traces_agg");
            putTransform.setHeader("Content-Type","application/json");
            putTransform.setEntity(new StringEntity(newJson));
            execHttp(httpclient, putTransform);

            HttpPost startTransform = new HttpPost(URL + "/_transform/traces_agg/_start");
            execHttp(httpclient, startTransform);
        }
    }

    private static void execHttp(CloseableHttpClient httpclient, HttpUriRequest request) {
        try (CloseableHttpResponse response = httpclient.execute(request)) {
            System.out.println(response.getStatusLine().toString());
            System.out.println(new Scanner(response.getEntity().getContent(),"utf-8").useDelimiter("\\Z").next());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
